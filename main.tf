# Using an existing resource group
#
data "azurerm_resource_group" "my-rg" {
  name     = var.var_resource_group
}

# Create a virtual network within the resource group
resource "azurerm_virtual_network" "network" {
  name                = "vnet-eastus-dev-tf_gitlab"
  address_space       = ["10.0.0.0/16"]
  location            = data.azurerm_resource_group.my-rg.location
  resource_group_name = data.azurerm_resource_group.my-rg.name
}

# Create a second virtual network within the resource group
resource "azurerm_virtual_network" "network2" {
  name                = "vnet-eastus-dev-tf2-aurelia"
  address_space       = ["10.0.0.0/16"]
  location            = data.azurerm_resource_group.my-rg.location
  resource_group_name = data.azurerm_resource_group.my-rg.name
}

# Add security group
resource "azurerm_network_security_group" "example" {

  name = "aurelialuciosecuritygroup"
  location = data.azurerm_resource_group.my-rg.location
  resource_group_name = data.azurerm_resource_group.my-rg.name

  security_rule {
    name = "test123"
    priority = 100
    direction = "Inbound"
    access = "Allow"
    protocol = "Tcp"
    source_port_range = "*"
    destination_port_range = "*"
    source_address_prefix = "*"
    destination_address_prefix = "*"
  }

  tags = {
    environment = "Development"
  }

}
