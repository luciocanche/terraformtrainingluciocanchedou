terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=2.46.0"
    }
  }


  //Backend local
  /*backend "local" {
    path = "../terraform.tfstate"
  }*/

  //Backend remoto
  backend "azurerm" {
    resource_group_name  = "rg_eastus_dev_terraform"
    storage_account_name = "sttfluciocanche"
    container_name       = "container-tf-dou"
    key                  = "terraform.tfstate"
  }
}